<?php
/**
 * Class that adds the wizard icon.
 *
 * @author	Steffen Kamper <steffen@sk-typo3.de>
 */
class tx_skpdfviewer_pi1_wizicon {
	/**
	 * Processing the wizard items array
	 *
	 * @param	array		$wizardItems: The wizard items
	 * @return	Modified array with wizard items
	 */
	function proc($wizardItems)	{
		global $LANG;
		$wizardItems['plugins_tx_skpdfviewer_pi1'] = array(
			'icon' => 'EXT:sk_pdfviewer/pi1/ce_wiz.gif',
			'title' => 'PDF Viewer',
			'description' => 'PDF Viewer',
			'params' => '&defVals[tt_content][CType]=list&defVals[tt_content][list_type]=sk_pdfviewer_pi1'
		);

		return $wizardItems;
	}
}

<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2006 Steffen Kamper <steffen@sk-typo3.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'PDF Viewer' for the 'sk_pdfviewer' extension.
 *
 * @author	Steffen Kamper <steffen@sk-typo3.de>
 */


class tx_skpdfviewer_pi1 extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
	var $prefixId = 'tx_skpdfviewer_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_skpdfviewer_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey = 'sk_pdfviewer';	// The extension key.
	var $pi_checkCHash = FALSE;
	var $CE_ID;       
    
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content,$conf)	{
		$this->conf=$conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		$this->pi_initPIflexForm();  
        
        $CE_ID=substr($this->cObj->currentRecord,11);     
        
        //process flexform
	    if($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'pdf_file', 'sPARAMS')!='') $this->conf['pdf_file'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'pdf_file', 'sPARAMS');   
        if($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'width', 'sPARAMS')!='') $this->conf['width'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'width', 'sPARAMS');   
        if($this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'height', 'sPARAMS')!='') $this->conf['height'] = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], 'height', 'sPARAMS');   

        $height = 1000;
        if(intval($this->conf['height'])) {
            $height = $this->conf['height'];
        }

        $width = 700;
        if(intval($this->conf['width'])) {
            $width = $this->conf['width'];
        }

	$file = 'uploads/tx_skpdfviewer/' . $this->conf['pdf_file'];

	return '<object type="application/pdf" data="' . htmlspecialchars($file) . '" width="' . $width . '" height="' . $height . '" style="background-color:white;"><p><a href="' . htmlspecialchars($file)  . '">' . htmlspecialchars($this->conf['pdf_file']) . '</a></p></object>';
	}
}

?>
